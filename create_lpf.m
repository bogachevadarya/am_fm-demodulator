function [numerators] = create_lpf(filter_order, wc)

k = (-filter_order/2+1:filter_order/2);
omega = 2*pi*k/filter_order;
for count = 1:filter_order;
   
 if abs(omega(count)/2/pi) <= wc   
     AMPL(count) = 1;
 else
     AMPL(count) = 0;
 end;
 
end;

 PHASE = -(omega)*(filter_order-1)/2;
 
 AMPL = fftshift(AMPL);
 PHASE = fftshift(PHASE);
 
 AMPL = circshift(AMPL,1);
 PHASE = circshift(PHASE,1);


 He = AMPL.*exp(j*PHASE);

 x = 0:1/filter_order:1-1/filter_order;
 z = 2*pi*x;
 Hamm = 0.54 - 0.46.*cos(z);
 
  
 for count = 1:filter_order
 filter_h(count) = sum(He.*exp(j*(2*pi*(0:filter_order-1).*(count-1)/filter_order)));
 end;

 filter_h = real(filter_h).*Hamm;
 numerators=filter_h/sum(filter_h);

 fvtool(numerators);
 
end